package cs2410.assn8.view;

import cs2410.assn8.controller.GameController;
import cs2410.assn8.controller.ViewController;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.util.ArrayList;

public class View extends Application
{

    /**
     * Height of the control panel/stat pane
     */
    private int controlPanelHeight = 60;

    /**
     * Current time spent playing
     */
    private static int time = 0;

    /**
     * Width of the cell pane/window
     */
    private int width = 250;

    /**
     * Height of the CELL PANE, not the height
     * of the entire window
     */
    private int height = 250;

    /**
     * Number of rows of cells.
     * I.E. How many cells in a column, going up and down
     */
    private static int numRows = 10;

    /**
     * Number of columns of cells
     * I.E. How many cells in a row, going across
     */
    private static int numCols = 10;

    /**
     * The current difficulty of the game, set to easy by default
     */
    private static GameController.Difficulty difficulty = GameController.Difficulty.EASY;

    /**
     * This tracks how many flags the user can still place. It does not necessarily
     * have the number of bombs remaining. Every time the user places a flag,
     * this decreases by one. Every time they remove a flag, it increases by one.
     */
    private static int bombCounter;

    /**
     * The timeline of the game. Changes the value of the time variable and
     * displays it every 1 second
     */
    private static Timeline timeline;

    /**
     * The Game Controller. Controls things such as winning, losing,
     * placing bombs, showing bombs, changing difficulty, showing all connecting cells
     * when a 0 has been clicked, detecting bombs around a cell, and resetting a game.
     */
    private GameController gameController;

    /**
     * View controller to manipulate objects from FXML layout
     */
    private static ViewController viewController;

    private enum Size{Small, Medium, Large};
    private enum GameMode {NORMAL, SPEED};

    private static GameMode gameMode = GameMode.NORMAL;

    private Size size = Size.Small;


    /**
     * Starts the program and initializes everything
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception
    {

        //load up the fxml outline
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Layout.fxml"));
        Parent root = loader.load();

        //scene
        Scene scene = new Scene(root, width, height+controlPanelHeight);

        //get the controller
        viewController = loader.getController();

        //set the background color of the stat pane, not visible in the game
        //purely for debuggging purposes
        viewController.getStatPane().setStyle("-fx-background-color: gainsboro;");

        //initialize the timeline
        //every 1 second the timeline increases the time spent playing by 1 second
        //and refreshes the time label
        timeline = new Timeline(new KeyFrame(Duration.seconds(1), event1 ->
        {
            time += 1;
            viewController.getTimeLabel().setText("Time:\n   " + String.valueOf(time));
        }));

        //keep it running while the program is active
        timeline.setCycleCount(Animation.INDEFINITE);

        //initialize the cells
        initializeCells();

        //initialize the top bar
        setStatBar();

        //initialize the menu items
        initializeMenuButtons(primaryStage);

        //set scene
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();

        primaryStage.setTitle("small - " + difficulty.name().toLowerCase());

        //rock and roll
        primaryStage.show();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("I Implemented the following features:\n\n1. Resizing feature(10 pts) - Fully functional.\n" +
                "2. Difficulty Feature(10 pts) - Fully Functional.\n3. Speed Demon Game mode(15 pts) - Fully Functional.\n\n" +
                "Note: Since I implemented both the resizing feature and difficulty feature, the game does not " +
                "resize when changing difficulty. This is by design. This allows the user to choose what size game " +
                "they want and then what difficulty they want at that size. Also, a bomb is added to the bombs remaining " +
                "number on the top left when a user changes from a flag to a question mark, not from a question mark to " +
                "unmarked. This is also be design, as it allows the user to still place question marks after they have " +
                "placed the max amount of flags, just like the real minesweeper game.");
        alert.showAndWait();
    }

    /**
     * Initializes all the cells in the cell pane and places them.
     * Depends on the number of cells in a row, column, as well as the
     * height and width of the cell pane. These values are specifically chosen
     * so that cells will be 20px x 20px on Medium and Large game modes,
     * and 25px x 25px on the Small game mode
     */
    public void initializeCells()
    {

        //arraylist of cells
        ArrayList<Cell> cells = new ArrayList<>(numCols*numRows);

        //create and place cells
        for(int y = 0; y < numRows; y++)
        {
            for(int x = 0; x < numCols; x++)
            {
                //create the cell, pass is the index number of the cell
                Cell cell = new Cell(String.valueOf((y*numCols)+x));

                //set the height and width of the cell
                cell.setPrefWidth(width/numCols);
                cell.setPrefHeight(height/numRows);

                //place the cell
                cell.setLayoutX((width/numCols)*x);
                cell.setLayoutY(height/numRows*y);

                //add the cell to the pane
                viewController.getCellPane().getChildren().add(cell);

                //add the new cell to the list of cells
                cells.add(cell);
            }
        }

        //cell pane holds the cells
        viewController.getCellPane().setLayoutY(controlPanelHeight);
        viewController.getCellPane().setPrefSize(width, height);

        //make the controller
        gameController = new GameController(cells, numCols, numRows, difficulty);

        //set the bomb counter
        bombCounter = gameController.getNumBombs();

        viewController.getCellPane().setStyle("-fx-background-color: #378bad");
    }

    /**
     * Initialzes and places everything in the stat bar
     */
    public void setStatBar()
    {
        //Bomb label
        viewController.getBombLabel().setText("Bombs Left:\n      " + String.valueOf(gameController.getNumBombs()));
        viewController.getBombLabel().setLayoutX((width/4)-35);
        viewController.getBombLabel().setLayoutY(controlPanelHeight/2 - 5);

        //Time Label
        viewController.getTimeLabel().setText("Time:\n   " + String.valueOf(time));
        viewController.getTimeLabel().setLayoutX(width*0.75 - 30);
        viewController.getTimeLabel().setLayoutY(controlPanelHeight/2 -5);

        //reset button
        viewController.getResetBtn().setLayoutY(controlPanelHeight/2);
        viewController.getResetBtn().setLayoutX(width/2 - 30);
        viewController.getResetBtn().setOnAction(gameController::resetGame);
    }

    /**
     * Sets the event handlers for all the menu items.
     * I.E. the items that change the size of the game and the difficulty
     * @param primaryStage the stage for the app
     */
    private void initializeMenuButtons(Stage primaryStage)
    {
        //set the small size button to true by default
        viewController.getSmallSizeBtn().setSelected(true);

        //set the event handler for the small size button
        viewController.getSmallSizeBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                resizeGame(primaryStage, 0);
                size = Size.Small;
                primaryStage.setTitle("small - " + difficulty.name().toLowerCase());
            }
        });

        //set the event handler for the medium size button
        viewController.getMedSizeBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                resizeGame(primaryStage, 1);
                size = Size.Medium;
                primaryStage.setTitle("med - " + difficulty.name().toLowerCase());
            }
        });

        //set the event handler for the large size button
        viewController.getLargeSizeBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                resizeGame(primaryStage, 2);
                size = Size.Large;
                primaryStage.setTitle("large - " + difficulty.name().toLowerCase());
            }
        });

        //set the easy button to true by default
        viewController.getEasyBtn().setSelected(true);

        //set the event handler for the easy button
        viewController.getEasyBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                difficulty = GameController.Difficulty.EASY;
                primaryStage.setTitle(size.name().toLowerCase() + " - easy");
                gameController.setGameDifficulty(GameController.Difficulty.EASY);
            }
        });

        //set the event handler for the medium button
        viewController.getMedBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                difficulty = GameController.Difficulty.MEDIUM;
                primaryStage.setTitle(size.name().toLowerCase() + " - med");
                gameController.setGameDifficulty(GameController.Difficulty.MEDIUM);
            }
        });

        //set the event handler for the hard button
        viewController.getHardBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                difficulty = GameController.Difficulty.HARD;
                primaryStage.setTitle(size.name().toLowerCase() + " - hard");
                gameController.setGameDifficulty(GameController.Difficulty.HARD);
            }
        });

        viewController.getNormalModeBtn().setSelected(true);
        //set the normal mode button
        viewController.getNormalModeBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event)
            {
                timeline.pause();
                timeline = new Timeline(new KeyFrame(Duration.seconds(1), event1 ->
                {
                    time += 1;
                    viewController.getTimeLabel().setText("Time:\n   " + String.valueOf(time));
                }));
                timeline.setCycleCount(Animation.INDEFINITE);

                gameMode = GameMode.NORMAL;
                gameController.resetGame(null);
            }
        });

        //set the speed mode button
        viewController.getSpeedModeBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                timeline.pause();
                timeline = new Timeline(new KeyFrame(Duration.seconds(1), event1 ->
                {
                    if(--time == 0)
                    {
                        Cell.gameOver();
                        GameController.gameOver(false);
                    }
                    viewController.getTimeLabel().setText("Time Remaining:\n   " + String.valueOf(time));
                }));
                timeline.setCycleCount(Animation.INDEFINITE);

                gameMode = GameMode.SPEED;
                gameController.resetGame(null);
            }
        });
    }


    /**
     * Decrements the bomb counter and refreshes the label showing how many
     * bombs are remaining. This occurs after a user right clicks a cell to place
     * a flag.
     */
    public static void decrementBombs()
    {
        bombCounter--;
        viewController.getBombLabel().setText("Bombs Left:\n      " + String.valueOf(bombCounter));
    }

    /**
     * Increments the bombs counter and refreshes the label showing how many
     * bombs are remaining. This occurs after a user right clicks a cell and
     * removes a flag.
     */
    public static void incrementBombs()
    {
        bombCounter++;
        viewController.getBombLabel().setText("Bombs Left:\n      " + String.valueOf(bombCounter));
    }

    /**
     * Get the current amount of bombs remaining (flags able to be placed)
     * @return the amount of flags able to be placed
     */
    public static int getBombCounter()
    {
        return bombCounter;
    }

    /**
     * Ends the current game. The timer stops, and an alert pops up
     * saying whether the user won or not.
     * @param won true if the user won, false if they lost
     */
    public static void endGame(boolean won)
    {
        //stop the timer
        timeline.pause();

        //make a new alert
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        //if they won
        if(won)
        {
            if(gameMode == GameMode.SPEED)
            {
                //happy you win alert
                alert.setContentText("Wow! You really are a Speed Demon!");
                alert.setHeaderText("You Win!");
            }
            else
            {
                //happy you win alert
                alert.setContentText("Congratulations! You finished the game in " + time + " seconds!");
                alert.setHeaderText("You Win!");
            }
        }
        //if not
        else
        {
            //sad you lose alert
            alert.setContentText("Oops! Try again!");
            alert.setHeaderText("You Lose!");
        }
        //show
        alert.show();
    }

    /**
     * starts the timer
     */
    public static void startTimer()
    {
        timeline.play();
    }

    /**
     * Resets the bomb label and the time label in the stat pane.
     * Happens when the user presses the reset button
     * @param controller the controller to find out how many bombs there are
     */
    public static void resetStats(GameController controller)
    {
        //pause the timer
        timeline.pause();
        //reset the bomb counter
        bombCounter = controller.getNumBombs();
        //reset the bomb label
        viewController.getBombLabel().setText("Bombs Left:\n      " + String.valueOf(bombCounter));

        if(gameMode == GameMode.NORMAL)
        {
            //set time back to 0
            time = 0;
            //reset the time counter
            viewController.getTimeLabel().setText("Time:\n   " + String.valueOf(time));
        }
        else if(gameMode == GameMode.SPEED)
        {
            time = 10;
            viewController.getTimeLabel().setText("Time Remaining:\n   " + String.valueOf(time));
        }
    }


    /**
     * Resizes the game screen.
     * Small is 10x10
     * Medium is 25x25
     * Large is 50x25
     * @param stage the stage to resize
     * @param size what to resize to. 0 = Small, 1 = Med, 2 = Large
     */
    private void resizeGame(Stage stage, int size)
    {
        //if resize to small
        if(size == 0)
        {
            //width and height are 250
            width = 250;
            height = 250;
            //10 cells up and down and across (25px each)
            numCols = 10;
            numRows = 10;
        }
        //if resize to medium
        else if(size == 1)
        {
            //width and height are 500
            width = 500;
            height = 500;
            //25 cells up and down and across(20px each)
            numCols = 25;
            numRows = 25;
        }
        //if resize to large
        else if(size == 2)
        {
            //width is 1000, height is 500
            width = 1000;
            height = 500;
            //50 cells across, 25 up and down (20px each)
            numCols = 50;
            numRows = 25;
        }

        //resize each of the panes to the new size
        viewController.getCellPane().setPrefSize(width, height);
        viewController.getStatPane().setPrefSize(width, controlPanelHeight);
        viewController.getRootPane().setPrefSize(width, height+controlPanelHeight);
        viewController.getMenuBar().setPrefWidth(width);

        //clear out all the old cells. We have to make all new cells
        //so that the indexes are correct for the new board
        viewController.getCellPane().getChildren().clear();

        //remake the cells
        initializeCells();

        //replace the stat bar
        setStatBar();

        //reset the time and bomb number
        resetStats(gameController);

        //reset the game state for the cells
        Cell.resetGame();

        //for some reason that im not entirely sure of,
        //when resizing the game window, even though it should be resizing to the
        //correct width and height, it cuts off around 3 pixels to right of the window
        //and the entire bottom row of cells.
        //Adding 3 to the width and 25 to the height seems to fix it though
        stage.setWidth(width+3);
        stage.setHeight(height+controlPanelHeight+25);
    }

    /**
     * Resets the time if playing speed demon
     */
    public static void resetSpeedTime()
    {
        if(gameMode == GameMode.SPEED)
            time = 11;
    }

}
