package cs2410.assn8.view;

import cs2410.assn8.controller.GameController;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class Cell extends Button
{
    /**
     * Is the Cell a bomb
     */
    private boolean isBomb;

    /**
     * Index of the cell
     */
    private int index;

    /**
     * How many bombs are neighboring this cell
     * Note. Even if the cell is a bomb it still
     * has a value for this, although it is not used
     */
    private int neighboringBombs;

    /**
     * Enum for different cell states
     * UNMARKED is when the cell is  not marked with a flag or question mark
     * FLAGGED is when the cell has been flagged
     * UNKNOWN is when the cell has been marked with a question mark
     */
    private enum CellState {UNMARKED, FLAGGED, UNKNOWN};

    /**
     * The Current state of the cell
     */
    private CellState currentState;

    /**
     * Has the cell been selected already or not
     */
    private boolean selected;

    /**
     * Is the game over
     */
    private static boolean gameOver = false;

    /**
     * Is it the first click of the game
     */
    private static boolean firstClick = true;

    /**
     * styling for an exploded bomb
     */
    private static String bombCSS = "-fx-background-color: red;" +
            "    -fx-border-width: 1px;" +
            "    -fx-font-size: 10px;" +
            "    -fx-border-radius: 0;" +
            "    -fx-background-radius: 0;" +
            "    -fx-border-color: lightgrey;" +
            "    -fx-padding: 0px;";

    /**
     * styling for a marked bomb
     */
    private static String markedBombCSS = "-fx-background-color: green;" +
            "    -fx-border-width: 1px;" +
            "    -fx-font-size: 10px;" +
            "    -fx-border-radius: 0;" +
            "    -fx-background-radius: 0;" +
            "    -fx-border-color: lightgrey;" +
            "     -fx-padding: 0px";

    /**
     * styling for an unclicked cell
     */
    private static String initialCSS = " -fx-border-width: 1;" +
            "-fx-font-size: 10px;" +
            "-fx-border-radius: 0;" +
            "-fx-background-radius: 0;" +
            "-fx-background-color: darkgrey;" +
            "-fx-border-color: black;" +
            "-fx-padding: 0px;";

    /**
     * style for a clicked cell
     */
    private static String clickedNonBombCSS = "-fx-background-color: cadetblue;" +
            "-fx-border-width: 1px;" +
            "-fx-font-size: 10px;" +
            "-fx-border-radius: 0px;" +
            "-fx-background-radius: 1px;" +
            "-fx-border-color: lightgrey;" +
            "-fx-padding: 0px;";

    /**
     * Styling for an incorrectly marked cell
     */
    private static String incorrectBombCSS = "-fx-background-color: yellow;" +
            "    -fx-border-width: 1px;" +
            "    -fx-font-size: 10px;" +
            "    -fx-border-radius: 0;" +
            "    -fx-background-radius: 0;" +
            "    -fx-border-color: lightgrey;" +
            "    -fx-padding: 0px";


    /**
     * Constructor for a cell
     * @param i the index of the cell
     */
    public Cell(String i)
    {
        //initialze the button
        super();

        //set the on mouse clicked action event
        //this one lets us determine which mouse button was pressed
        super.setOnMouseClicked(this::mouseClicked);


        //set the initial style of the button
        super.setStyle(initialCSS);

        //make the cell not a bomb by default
        isBomb = false;

        //the cell hasnt been selected yet
        selected = false;

        //set the number of neighboringBombs to 0 by default
        neighboringBombs = 0;

        //set the default state as unmarked
        currentState = CellState.UNMARKED;

        index = Integer.parseInt(i);


    }

    /**
     * Makes the current cell a bomb
     */
    public void makeBomb()
    {
        isBomb = true;
        /**
         * Uncomment this to make bombs visible
         *
         * Cell.super.setStyle("-fx-background-color: red;-fx-border-width: 1px;-fx-font-size: 8px;" +
         " -fx-border-radius: 0; -fx-background-radius: 0");
         */
    }

    /**
     * makes the current cell not a bomb
     */
    public void defuseBomb()
    {
        isBomb = false;
        super.setStyle(" -fx-border-width: 1;-fx-font-size: 10px;" +
                " -fx-border-radius: 0; -fx-background-radius: 0; -fx-background-color: darkgrey;" +
                "-fx-border-color: black");
    }

    /**
     * checks if the cell is a bomb or not
     * @return if the cell is a bomb or not
     */
    public boolean isBomb()
    {
        return isBomb;
    }

    /**
     * increases the amount of neighboring bombs to this cell
     */
    public void increaseNeighboringBombs()
    {
        neighboringBombs++;
    }

    /**
     * resets the neighboring bomb count to 0
     */
    public void reset()
    {
        neighboringBombs = 0;
        super.setText("");
        setGraphic(null);
        defuseBomb();
        currentState = CellState.UNMARKED;
        selected = false;
    }

    /**
     * getter for neighboring bombs
     * @return the number of bombs neighboring this cell
     */
    public int getNeighboringBombs()
    {
        return neighboringBombs;
    }

    /**
     * changes the color of the cell for a bomb
     */
    public void showBomb()
    {
        //if the bomb was detected
        if(currentState == CellState.FLAGGED)
            //make it green
            setStyle(markedBombCSS);
        //if not
        else
            //make it red
            setStyle(bombCSS);

        setGraphic(new ImageView(new Image("file:data/bomb.png")));
    }

    /**
     * changes the color of a cell incorrectly marked as a bomb
     */
    public void showIncorrectlyMarked()
    {
        setStyle(incorrectBombCSS);
    }

    /**
     * resets the state of the game for ALL CELLS, not just the individual cell.
     */
    public static void resetGame()
    {
        gameOver = false;
        firstClick = true;
    }

    /**
     * Event Handler for a mouse click. A left mouse click activates the cell,
     * a right mouse click changes the state of the cell.
     * @param event
     */
    private void mouseClicked(MouseEvent event)
    {
        //if we've already selected this button then dont fire
        if(selected)
            return;

        //start the timer if its the first click
        if(firstClick)
        {
            View.startTimer();
            firstClick = false;
        }

        //if it was a right click
        if(event.getButton() == MouseButton.SECONDARY && !gameOver)
        {
            //switch depending on the current state of the cell
            switch (currentState)
            {
                //if its currently unmarked
                case UNMARKED:
                    //if the bomb counter is 0, we dont want the user to be able to
                    //place anymore flags
                    if(View.getBombCounter() == 0)
                    {
                        //so skip the marked state and go straight to unknown
                        currentState = CellState.UNKNOWN;
                        setGraphic(new ImageView(new Image("file:data/questionmark.png")));
                    }
                    //if there are still more flags that are able to be placed
                    else
                    {
                        //set the image to a flag
                        setGraphic(new ImageView(new Image("file:data/flag.png")));
                        //add the cell to the flaggedCells tree in the game controller
                        GameController.addMarkedCell(index);
                        //decrement the amount of bombs remaining in the GUI
                        View.decrementBombs();

                        //if the cell is also a bomb
                        if(isBomb)
                            //decrement the amount of bombs remaining
                            GameController.flagBomb();

                        //change the cell state to flagged
                        currentState = CellState.FLAGGED;
                    }
                    break;
                //if the cell is currently flagged
                case FLAGGED:
                    //change it to a question mark
                    setGraphic(new ImageView(new Image("file:data/questionmark.png")));
                    //remove the cell from the list of cells that have been flagged
                    GameController.removedFlaggedCell(index);

                    //if the cell is also a bomb, add 1 to the number of bombs remaining
                    if(isBomb)
                        GameController.unflagBomb();

                    //increase the number of remaining bombs in the gui
                    View.incrementBombs();
                    //set the cell state to unknown
                    currentState = CellState.UNKNOWN;
                    break;
                //if the cell is currently unknown
                case UNKNOWN:
                    //clear the text and image
                    setText("");
                    setGraphic(null);
                    //set the cell state to unmarked
                    currentState = CellState.UNMARKED;
                    break;
            }
        }
        //if it was a left click
        else if(event.getButton() == MouseButton.PRIMARY && !gameOver)
        {
            //make sure its valid to be clicked on
            if(currentState == CellState.UNMARKED)
            {
                //check first if its a bomb
                if(isBomb == true)
                {
                    //if so then set the style to red
                    setStyle(bombCSS);
                    //remove any text
                    setText("");
                    //set the game to over
                    gameOver = true;
                    //show all the bombs
                    GameController.gameOver(false);
                }
                //if its not a bomb
                else
                {
                    //reset the time on the clock if we're playing speed demon
                    View.resetSpeedTime();
                    //set the style to clicked
                    Cell.super.setStyle(clickedNonBombCSS);
                    //if its a cell with no neighboring bombs, start a recursive method to show
                    //all the empty cells connected to this cell
                    if(neighboringBombs == 0)
                        GameController.showConnectingZeroes(index);
                        //if not then set the text to the number of bombs touching this cell
                    else
                        Cell.super.setText(String.valueOf(neighboringBombs));
                }
            }
            selected = true;
        }
        //this only runs for the recursive showConnectingZeroesMethod
        else
        {

            if(currentState == CellState.UNMARKED && !gameOver && !selected)
            {
                GameController.removeCell(index);


                Cell.super.setStyle(clickedNonBombCSS);
                if(neighboringBombs != 0)
                    Cell.super.setText(String.valueOf(neighboringBombs));
            }
        }
    }

    /**
     * Get the index of this particular cell
     * @return the index
     */
    public int getIndex()
    {
        return index;
    }

    /**
     * set the selected attribute to true
     */
    public void setSelected()
    {
        selected = true;
    }

    /**
     * Find out if the cell has been selected or not
     * @return whether its been selected
     */
    public boolean isSelected()
    {
        return selected;
    }

    /**
     * Normally the fire method activates the onAction Event handler,
     * however this overrides it and creates a MouseClicked event.
     * This is used for the recursive function showConnectingZeroes
     */
    @Override
    public void fire()
    {
        fireEvent(new MouseEvent(MouseEvent.MOUSE_CLICKED,
                0, 0, 0, 0, MouseButton.MIDDLE, 1,
                true, true, true, true,
                false, true, false,
                true, true, true, null));
    }


    /**
     * Makes the user unable to select cells after a game is over
     */
    public static void gameOver()
    {
        gameOver = true;
    }

}
