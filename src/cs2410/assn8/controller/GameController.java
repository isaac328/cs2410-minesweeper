package cs2410.assn8.controller;
import cs2410.assn8.view.*;
import javafx.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.TreeSet;

public class GameController
{
    /**
     * All the cells in the game. This ArrayList is not altered unless the size of the game is changed
     */
    private static ArrayList<Cell> cells;
    /**
     * All the bombs in the current game. This list is set at the beginning of each game
     * and does not change until a new game is started
     */
    private static ArrayList<Integer> bombs;

    //these three fields are just for information keeping and
    //for calculating indexes
    private static int numRows;
    private static int numCols;
    private static int numBombs;

    /**
     * number of und
     */
    private static TreeSet<Integer> cellsRemaining;

    /**
     * All cells that have been FLAGGED, whether they are bombs or not.
     * Indexes are dynamically added and removed from this tree throughout the game.
     * When a cell is marked with a flag, it is added to this list. If they change the
     * flag to a question mark or remove a marking all together, it is removed from this list.
     * The main function of this list is to know which cells have been flagged, so at the end of
     * the game they can be highlighed accordingly.
     */
    private static TreeSet<Integer> flaggedCells;

    /**
     *  Number of bombs that have not yet been flagged. May or may not be the same as the
     *  bombs remaining number on the stat pane.
     */
    private static int bombsRemaining;

    public enum Difficulty {EASY, MEDIUM, HARD};

    private Difficulty gameDifficulty;

    public GameController(ArrayList<Cell> cells, int numCols, int numRows, Difficulty difficulty)
    {
        this.cells = cells;
        this.numCols = numCols;
        this.numRows = numRows;

        //determine how many bombs there are
        switch(difficulty)
        {
            case EASY:
                numBombs = (int)Math.floor(numCols*numRows * 0.1);
                break;
            case MEDIUM:
                numBombs = (int)Math.floor(numCols*numRows*0.25);
                break;
            case HARD:
                numBombs = (int)Math.floor(numCols*numRows*0.5);
                break;
        }

        flaggedCells = new TreeSet<>();
        cellsRemaining = new TreeSet<>();
        bombsRemaining = numBombs;

        gameDifficulty = Difficulty.EASY;

        //add all the cells to the not bomb cells list
        for(Cell c : cells)
            cellsRemaining.add(c.getIndex());

        //set the game
        setBombs();
    }

    /**
     * resets the game
     * @param event
     */
    public void resetGame(ActionEvent event)
    {
        //clear the not Bomb cell tree and then add them all back in
        cellsRemaining.clear();

        for(Cell c : cells)
        {
            c.reset();
            cellsRemaining.add(c.getIndex());
        }

        View.resetStats(this);
        Cell.resetGame();
        flaggedCells.clear();
        bombsRemaining = numBombs;

        setBombs();

    }

    /**
     * Sets all the bombs for the game
     *
     * Randomly chooses indexes within the boundry of [0, numCols * numRows)
     * keeps randomly choosing until it finds a cell that isnt already a bomb
     * designates that cell a bomb, and then calls the incrementNeighbors method to increase the bomb count
     * of all the surrounding cells.
     * Keeps going all the bombs have been placed
     */
    private void setBombs()
    {
        bombs = new ArrayList<>(numBombs);

        Random rand = new Random();
        for(int i = 0; i < numBombs; i++)
        {
            //compute random index from [0, number of columuns * number of rows)
            int cellIndex = rand.nextInt(numRows*numCols);

            //get the cell at that index
            Cell cell = cells.get(cellIndex);

            //if the cell we randomly picked is already a bomb then keep picking random
            //cells until we find one that isn't already a bomb.
            //With a bomb rate of roughly 25% this should be pretty fast
            while(cell.isBomb())
            {
                cellIndex = rand.nextInt(numRows*numCols);
                cell = cells.get(cellIndex);
            }

            //once we find one that isn't already a bomb, make it a bomb;
            cell.makeBomb();
            bombs.add(cellIndex);

            //remove it from the cells remaining list
            cellsRemaining.remove(cellIndex);

            //increase the neighboringBomb count of all the neighboring cells
            incrementNeighbors(cellIndex);
        }
    }

    /**
     * Increments the neighboringBomb count of all the cells surrounding a bomb (index).
     * There are several different cases to check for so that an ArrayIndexOutOfBounds Exception is not thrown.
     * First check if the index is a corner index.
     * Then check if its an index on the left, right, top, or bottom walls.
     * If its none of those things then it falls to the final condition
     * @param index the bomb
     */
    private void incrementNeighbors(int index)
    {
        //all special cases for cells
        boolean topLeftCorner = (index == 0);
        boolean topRightCorner = (index == (numCols-1));
        boolean bottomLeftCorner = (index == ((numRows * numCols) - numCols));
        boolean bottomRightCorner = (index == (numCols * numRows) - 1);
        boolean leftBorder = (index % numCols == 0);
        boolean rightBorder = (index % numCols == (numCols - 1));
        boolean topBorder = (index > 0 && index < numCols);
        boolean bottomBorder = ((index > (numRows * numCols) - numCols) && (index < (numCols * numRows) - 1));

        //check corner cases first
        //if its the top left one or bottom left
        if(topLeftCorner || bottomLeftCorner)
        {
            //this loop only does our current column and the one to the right
            for(int i = 0; i <=1; i++)
            {
                //increment all the cells in the one current row
                cells.get(index + i).increaseNeighboringBombs();
                //cells.get(index + i).resetText();

                if(topLeftCorner)
                {
                    //increment all the cells in the row below the current one
                    cells.get(index + numCols + i).increaseNeighboringBombs();
                    //cells.get(index + numCols + i).resetText();
                }
                else
                {
                    //increment all the cells in the row above the current one
                    cells.get(index - numCols + i).increaseNeighboringBombs();
                    //cells.get(index - numCols + i).resetText();
                }
            }
        }
        //if its the top right or bottom right
        else if(topRightCorner || bottomRightCorner)
        {
            //this loop does the current column and the one to the left
            for(int i = -1; i <= 0; i++)
            {
                //increment all the cells in the one current row
                cells.get(index + i).increaseNeighboringBombs();
                //cells.get(index + i).resetText();

                if(topRightCorner)
                {
                    //increment all the cells in the row below the current one
                    cells.get(index + numCols + i).increaseNeighboringBombs();
                    //cells.get(index + numCols + i).resetText();
                }
                else
                {
                    //increment all the cells in the row above the current one
                    cells.get(index - numCols + i).increaseNeighboringBombs();
                    //cells.get(index - numCols + i).resetText();
                }
            }
        }

        //if its on the left or right border
        else if(leftBorder || rightBorder)
        {
            //this loop lets you access surrounding cells
            for(int i = -1; i <= 1; i++)
            {
                int neighborIndex = index + (i * numCols);
                cells.get(neighborIndex).increaseNeighboringBombs();
                //cells.get(neighborIndex).resetText();

                //if its a left border increase all the cells to the right
                if(leftBorder)
                {
                    cells.get(index + (i * numCols) + 1).increaseNeighboringBombs();
                    //cells.get(index + (i * numCols) + 1).resetText();
                }
                //if its a right border cell increase all the cells to the left
                else
                {
                    cells.get(index + (i * numCols) - 1).increaseNeighboringBombs();
                    //cells.get(index + (i * numCols) - 1).resetText();
                }
            }

        }
        //if its on the top or bottom row
        else if(topBorder || bottomBorder)
        {
            for(int i = -1; i <= 1; i++)
            {
                //increase the cells to the left and right
                cells.get(index + i).increaseNeighboringBombs();
                //cells.get(index + i).resetText();

                //if its on the top row increase the ones below it
                if(topBorder)
                {
                    cells.get(index + i + numCols).increaseNeighboringBombs();
                    //cells.get(index + i + numCols).resetText();
                }
                //if its on the bottom row increase the ones above it
                else
                {
                    cells.get(index + i - numCols).increaseNeighboringBombs();
                    //cells.get(index + i - numCols).resetText();
                }
            }
        }
        // if its just a regular cell in the middle
        else
        {
            for(int i = -1; i <= 1; i++)
            {
                //increment all the cells in the row above the current one
                cells.get(index - numCols + i).increaseNeighboringBombs();
                //cells.get(index - numCols + i).resetText();

                //increment all the cells in the row below the current one
                cells.get(index + numCols + i).increaseNeighboringBombs();
                //cells.get(index + numCols + i).resetText();

                //increment all the cells in the one current row
                cells.get(index + i).increaseNeighboringBombs();
                //cells.get(index + i).resetText();
            }
        }
    }

    /**
     * This is the wrapper function for the recursive method ShowConnectingZeroes
     * Initializes the HashSet that stores already visited nodes and enters the recursive method
     * @param index the index to start with
     */
    public static void showConnectingZeroes(int index)
    {
        //initialize the hashset
        HashSet<Integer> visitedNodes = new HashSet<>();
        //go to the main recursive method
        showConnectingZeroes(index, visitedNodes);

    }

    /**
     * This is a recursive method that checks all the cells around a current cell for a neighboringBombs value of 0.
     * If a neighboring cell has 0 bombs touching it, and has not been already visited, we go to that cell,
     * fire it, and check all of its neighboring cells. This is a depth first approach, so we will start and end on the
     * first cell. Since we are checking all the cells surrounding a certain index, we still need to test for special cases
     * to avoid an ArrayIndexOutOfBoundsException
     *
     * @param index the current cell index
     * @param visitedNodes a HashSet of all the nodes that have already been visited
     */
    private static void showConnectingZeroes(int index, HashSet<Integer> visitedNodes)
    {
        //add the current node the the visited list
        visitedNodes.add(index);

        //fire the current button so it shows up
        cells.get(index).fire();

        //mark the cell selected
        cells.get(index).setSelected();

        //all special cases for cells
        boolean topLeftCorner = (index == 0);
        boolean topRightCorner = (index == (numCols-1));
        boolean bottomLeftCorner = (index == ((numRows * numCols) - numCols));
        boolean bottomRightCorner = (index == (numCols * numRows) - 1);
        boolean leftBorder = (index % numCols == 0);
        boolean rightBorder = (index % numCols == (numCols - 1));
        boolean topBorder = (index > 0 && index < numCols);
        boolean bottomBorder = ((index > (numRows * numCols) - numCols) && (index < (numCols * numRows) - 1));

        //check corner cases first
        //if its the top left one or bottom left
        if(topLeftCorner || bottomLeftCorner)
        {
            //this loop only does our current column and the one to the right
            for(int i = 0; i <=1; i++)
            {
                //test the cells in the same row first
                //if they have 0 neighboring bombs and they havent been visited already then go there
                if(cells.get(index + i).getNeighboringBombs() == 0 && !visitedNodes.contains(index))
                    //recurse through the next button
                    showConnectingZeroes(index + i, visitedNodes);
                else if(cells.get(index + i).getNeighboringBombs() != 0)
                    cells.get(index + i).fire();

                //next index to be tested
                int indexToBeTested;
                //if its the Top left corner we want to test the cells below it
                if(topLeftCorner)
                    indexToBeTested = index + numCols + i;
                    //if its the bottom left corner we want to test the cells above it
                else
                    indexToBeTested = index - numCols + i;

                //check if the cell is a zero and if its already been visited
                if(cells.get(indexToBeTested).getNeighboringBombs() == 0 && !visitedNodes.contains(indexToBeTested))
                    //recurse through the next button
                    showConnectingZeroes(indexToBeTested, visitedNodes);

                else if(cells.get(indexToBeTested).getNeighboringBombs() != 0)
                    cells.get(indexToBeTested).fire();
            }
        }
        //if its the top right or bottom right
        else if(topRightCorner || bottomRightCorner)
        {
            //this loop does the current column and the one to the left
            for(int i = -1; i <= 0; i++)
            {
                //test the cells in the same row first
                //if they have 0 neighboring bombs and they havent been visited already then go there
                if(cells.get(index + i).getNeighboringBombs() == 0 && !visitedNodes.contains(index))
                    //recurse through the next button
                    showConnectingZeroes(index + i, visitedNodes);

                    //if its not a zero then fire it
                else if(cells.get(index + i).getNeighboringBombs() != 0)
                    cells.get(index + i).fire();

                int indexToBeTested;
                //if its in the top right corner we want to test the cells below it
                if(topRightCorner)
                    indexToBeTested = index + numCols + i;

                    //if its in the bottom right corner we want to test the cells above it
                else
                    indexToBeTested = index - numCols + i;

                //test the cells above or below it
                if(cells.get(indexToBeTested).getNeighboringBombs() == 0 && !visitedNodes.contains(index))
                    showConnectingZeroes(indexToBeTested, visitedNodes);

                    //if its not a zero then fire it
                else if(cells.get(indexToBeTested).getNeighboringBombs() != 0)
                    cells.get(indexToBeTested).fire();
            }
        }

        //if its on the left or right border
        else if(leftBorder || rightBorder)
        {
            //this loop lets you access surrounding cells
            for(int i = -1; i <= 1; i++)
            {
                //do the cells above and below it first
                int indexToBeTested = index + (i * numCols);

                //test if it has a zero and if its been visited already
                if(cells.get(indexToBeTested).getNeighboringBombs() == 0 && !visitedNodes.contains(indexToBeTested))
                    showConnectingZeroes(indexToBeTested, visitedNodes);

                    //if its not a zero then fire it
                else if(cells.get(indexToBeTested).getNeighboringBombs() != 0)
                    cells.get(indexToBeTested).fire();

                //if its a left border we want to test the cells on the right
                if(leftBorder)
                    indexToBeTested = index + (i * numCols) + 1;

                    //if its a right border we want to test the cells to its left
                else
                    indexToBeTested = index + (i * numCols) - 1;

                //next test the cells to the left or right depending on what side its on
                //test if it has a zero and if its been visited already
                if(cells.get(indexToBeTested).getNeighboringBombs() == 0 && !visitedNodes.contains(indexToBeTested))
                    showConnectingZeroes(indexToBeTested, visitedNodes);

                    //if its not a zero then fire it
                else if(cells.get(indexToBeTested).getNeighboringBombs() != 0)
                    cells.get(indexToBeTested).fire();
            }

        }
        //if its on the top or bottom row
        else if(topBorder || bottomBorder)
        {
            for(int i = -1; i <= 1; i++)
            {
                //first check the cells to the left or right
                if(cells.get(index + i).getNeighboringBombs() == 0 && !visitedNodes.contains(index + i))
                    showConnectingZeroes(index + i, visitedNodes);

                    //if its not a zero then fire it
                else if(cells.get(index + i).getNeighboringBombs() != 0)
                    cells.get(index + i).fire();


                int indexToBeTested;
                //if its on the top row we want to check the ones below it
                if(topBorder)
                    indexToBeTested = index + i + numCols;

                    //if its on the bottom row we want to check the ones above it
                else
                    indexToBeTested = index + i - numCols;

                //test the cells above or below
                if(cells.get(indexToBeTested).getNeighboringBombs() == 0 && !visitedNodes.contains(indexToBeTested))
                    showConnectingZeroes(indexToBeTested, visitedNodes);

                else if(cells.get(indexToBeTested).getNeighboringBombs() != 0)
                    cells.get(indexToBeTested).fire();
            }
        }
        // if its just a regular cell in the middle
        else
        {
            for(int i = -1; i <= 1; i++)
            {
                int indexToBeTested = index - numCols + i;

                //test the cells above our current cell
                if(cells.get(indexToBeTested).getNeighboringBombs() == 0 && !visitedNodes.contains(indexToBeTested))
                    showConnectingZeroes(indexToBeTested, visitedNodes);

                else if(cells.get(indexToBeTested).getNeighboringBombs() != 0)
                    cells.get(indexToBeTested).fire();


                //test the cells below our current cell
                indexToBeTested = index + numCols + i;
                if(cells.get(indexToBeTested).getNeighboringBombs() == 0 && !visitedNodes.contains(indexToBeTested))
                    showConnectingZeroes(indexToBeTested, visitedNodes);

                else if(cells.get(indexToBeTested).getNeighboringBombs() != 0)
                    cells.get(indexToBeTested).fire();

                //increment all the cells in the one current row
                indexToBeTested = index + i;
                if(cells.get(indexToBeTested).getNeighboringBombs() == 0 && !visitedNodes.contains(indexToBeTested))
                    showConnectingZeroes(indexToBeTested, visitedNodes);

                else if(cells.get(indexToBeTested).getNeighboringBombs() != 0)
                    cells.get(indexToBeTested).fire();
            }
        }
    }

    /**
     * Ends the game, shows all bombs
     */
    public static void gameOver(boolean win)
    {
        //show all the bombs
        for(int i : bombs)
            cells.get(i).showBomb();

        //show all the incorrectly marked cells
        for(int i : flaggedCells)
        {
            if(!cells.get(i).isBomb())
                cells.get(i).showIncorrectlyMarked();
        }

        View.endGame(win);
    }

    /**
     * Adds a marked cell (a cell that has been marked with a flag or question mark)
     * to the flaggedCells TreeMap
     * @param index
     */
    public static void addMarkedCell(int index)
    {
        flaggedCells.add(index);
    }

    /**
     * removes a marked cell (a cell that been marked with a flag or question mark)
     * to the flaggedCells TreeMap
     * @param index
     */
    public static void removedFlaggedCell(int index)
    {
        flaggedCells.remove(index);
    }

    /**
     * removes a cell index from the notBombCells TreeMap
     * @param index the index to remove
     */
    public static void removeCell(int index)
    {
        cellsRemaining.remove(index);
        if(cellsRemaining.isEmpty())
            gameOver(true);

        //System.out.println(notBombCells.size());
    }

    /**
     * When a bomb is correctly flagged, the amount of bombs needing to
     * be flagged goes down by one. This is not called every time the user
     * places a flag, only when they CORRECTLY place a flag. This allows
     * us to end the game when all bombs have been correctly flagged
     */
    public static void flagBomb()
    {
        bombsRemaining--;
        if(bombsRemaining == 0)
            gameOver(true);
    }

    /**
     * When a correctly  flagged bomb is unflagged, the amount of bombs
     * needing to be flagged is increased by 1. This is not called every time
     * the user unflags a cell, only when they unflag a cell that is a bomb.
     */
    public static void unflagBomb() {
        bombsRemaining++;
    }

    /**
     * Sets the difficulty of the game and then determines how many bombs
     * there needs to be depending on the difficulty and size of the board.
     * Easy = 10% of the board is bombs
     * Med  = 25% of the board is bombs
     * Hard = 50% of the board is bombs
     * @param difficulty the new difficulty
     */
    public void setGameDifficulty(Difficulty difficulty)
    {
        //set the difficulty
        this.gameDifficulty = difficulty;

        //figure out how many bombs we need depending on the difficulty
        switch(difficulty)
        {
            //easy is 10%
            case EASY:
                numBombs = (int)Math.floor(numCols*numRows * 0.1);
                break;
            //medium is 25%
            case MEDIUM:
                numBombs = (int)Math.floor(numCols*numRows*0.25);
                break;
            //hard is 50%
            case HARD:
                numBombs = (int)Math.floor(numCols*numRows*0.5);
                break;
        }

        //reset the game after we change the number of bombs
        resetGame(null);
    }

    /**
     * Getter for the current number of bombs for this match
     * (Total number of bombs, not how many are remaining or need to be flagged)
     * @return the number of bombs
     */
    public int getNumBombs()
    {
        return numBombs;
    }
}
