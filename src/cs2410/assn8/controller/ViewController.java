package cs2410.assn8.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

public class ViewController
{
    @FXML
    private Pane statPane;

    @FXML
    private Pane cellPane;

    @FXML
    private Pane rootPane;

    @FXML
    private Button resetBtn;

    @FXML
    private Label bombLabel;

    @FXML
    private Label timeLabel;

    @FXML
    private RadioMenuItem smallSizeBtn;

    @FXML
    private RadioMenuItem medSizeBtn;

    @FXML
    private  RadioMenuItem largeSizeBtn;

    @FXML
    private MenuBar menuBar;

    @FXML
    private RadioMenuItem easyBtn;

    @FXML
    private RadioMenuItem medBtn;

    @FXML
    private RadioMenuItem hardBtn;

    @FXML
    private RadioMenuItem normalModeBtn;

    @FXML
    private RadioMenuItem speedModeBtn;

    public Pane getStatPane()
    {
        return statPane;
    }

    public Pane getCellPane()
    {
        return cellPane;
    }

    public Pane getRootPane() {
        return rootPane;
    }

    public Button getResetBtn()
    {
        return resetBtn;
    }

    public Label getBombLabel()
    {
        return bombLabel;
    }

    public Label getTimeLabel()
    {
        return timeLabel;
    }

    public RadioMenuItem getLargeSizeBtn() {
        return largeSizeBtn;
    }

    public RadioMenuItem getMedSizeBtn() {
        return medSizeBtn;
    }

    public RadioMenuItem getSmallSizeBtn() {
        return smallSizeBtn;
    }

    public RadioMenuItem getEasyBtn() {
        return easyBtn;
    }

    public RadioMenuItem getMedBtn() {
        return medBtn;
    }

    public RadioMenuItem getHardBtn() {
        return hardBtn;
    }

    public MenuBar getMenuBar() {
        return menuBar;
    }

    public RadioMenuItem getNormalModeBtn() {
        return normalModeBtn;
    }

    public RadioMenuItem getSpeedModeBtn() {
        return speedModeBtn;
    }
}
